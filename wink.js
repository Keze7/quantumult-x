
/*******************************
应用名称：Wink
脚本功能：解锁会员
应用版本：1.5.00
应用下载：App Store
脚本作者：Kaze
更新时间：2023-06-28
脚本发布：暂无
使用声明：‼️仅供学习交流, 🈲️商业用途
*******************************
[rewrite_local]
^https:\/\/api-sub\.meitu\.com\/v2\/user\/vip_info_by_group\.json\? url script-response-body https://gitlab.com/Keze7/quantumult-x/-/new/main/wink.js 
[MITM]
hostname = api-sub.meitu.com

*******************************/
let bodyJson = JSON.parse($response.body);
bodyJson.data = {
"account_id" : "1932334474",
    "is_vip" : true,
    "limit_type" : 0,
    "have_valid_contract" : false,
    "use_vip" : true,
    "show_renew_flag" : false,
    "in_trial_period" : false,
    "account_type" : 1
};
$done({body: JSON.stringify(bodyJson)}); 
